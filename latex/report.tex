\documentclass[a4paper,12pt]{article}
\usepackage{graphicx}
\usepackage{amsmath, amsfonts}
\usepackage{amssymb}

\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{lmodern}
\usepackage{textcomp}

\usepackage{natbib}

% \usepackage{babel}

% \usepackage{titlesec}

\usepackage[top=1.2in,bottom=1.2in,left=3.cm,right=3.cm,a4paper]{geometry}

\usepackage[font=footnotesize]{caption}

\usepackage{xcolor}

\usepackage[colorlinks=true, allcolors=blue]{hyperref}

\hypersetup{
    colorlinks,
    linkcolor={red!50!black},
    citecolor={blue!50!black},
    urlcolor={blue!99!black}
}

\usepackage{titling}

\pretitle{%
\includegraphics[width=0.3\linewidth]{../fig/logo_UGA}
\vspace{2cm}
\begin{center}
\LARGE
}
\posttitle{\end{center}}
\postdate{\par\end{center}\vspace{12cm}~}

\usepackage[nottoc,numbib]{tocbibind}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{Introduction to some turbulence problems}
\author{Antonin Bernard}

% to define new commands
\newcommand{\R}{\mathcal{R}}
\newcommand{\mean}[1]{\langle #1 \rangle}

% to show the pieces of advice
\newcommand{\advice}[1]{{\it #1}}
% to hide the pieces of advice
% \newcommand{\advice}[1]{}

\begin{document}

\renewcommand{\labelitemi}{$\bullet$}

\maketitle

\newpage

\tableofcontents

\newpage


\begin{abstract}
	
Instabilities and turbulences in fluids are deep and important phenomenons that can be observed in a lot of flows surrounding us. Throughout this paper, thanks to two common examples, one will demonstrate the importance of instabilities in flows and their analysis. Through the analysis of a dynamical system, Lotka Volterra equations, one will show that instabilities are very meaningful for sensible systems. Then, one everyday life turbulence phenomenom will be discussed, with this discussion one will try to extract general properties of turbulent flows; 


\end{abstract}

\newpage

\section{Introduction to instabilities and tuburlence in fluids}



For a \textbf{stable flow}, any infinitely small variation, which is considered a disturbance, will not have any noticeable effect on the initial state of the system and will eventually die down in time. For a fluid flow to be considered stable it must be stable with respect to every possible disturbance. This implies that there exists no mode of disturbance for which it is unstable. \\ \\
On the other hand, for an \textbf{unstable flow}, any variations will have some noticeable effect on the state of the system which would then cause the disturbance to grow in amplitude in such a way that the system progressively departs from the initial state and never returns to it. This means that there is at least one mode of disturbance with respect to which the flow is unstable, and the disturbance will therefore distort the existing force equilibrium. \\

The governing equation of fluid mechanics is the famous \textbf{Navier Stokes equation}, that we recall here : \\

\begin{center}
$ D_{t}(\vec{u}) = -\vec{\nabla}{\vec{P}} + \nu \Delta\vec{u}$
\end{center}

Where : 

\begin{itemize}
	\item $\vec{u}$ is the velocity of the flow 
	\item $ D_{t}$ is a non linear term called the Lagrangian derivative and when used on $\vec{u}$ it represents the acceleration of the flow;
	\item $P$ is the pressure;
	\item $\nu$ is the kinematic viscosity
\end{itemize}


One can solve this set of equations and find solutions; when one inspects the stability (steady or unsteady) of these solutions, one is able to determine the instabilities resulting. \\ 

Concerning turbulences, a system can be characterized as \textbf{turbulent} when structures with a broad range of scales interact non-linearly in a system with several degrees of freedom. For example when an object of a particular size is placed into a flow of a different size and velocity, then when the velocity is high enough we are entering the turbulent regime; one way to inspect that is the Reynolds number.

\newpage


\subsection{Reynolds Number}

The Reynolds number, in the simplest way, is defined as follow : 
\begin{center}
	$ R_{e} = \dfrac{UL}{\nu} $
\end{center}

With : 
\begin{itemize}
	\item U is the velocity of the fluid considered;
	\item L is a characteristic length;
	\item $\nu$ is the kinematic viscosity.
\end{itemize}


This is a key tool used to determine the \textbf{regime} of a flow (laminar, transition or turbulent). This dimensionless number gives the ratio of inertial terms over viscous terms. In a physical sense, this number is a ratio of the forces which are due to the momentum of the fluid (inertial terms), and the forces which arise from the relative motion of the different layers of a flowing fluid (viscous terms). We can then rewrite it using the Navier Stokes terms : 

\begin{center}
	$ R_{e} = \dfrac{|\vec{u}.\vec{\nabla}\vec{u}|}{|\nu\Delta\vec{u}|} $
\end{center}


Using this definition, the Reynolds number is useful because it can provide cut off points for when flow is stable or unstable, namely the Critical Reynolds number, $R_{c}$ As it increases, the amplitude of a disturbance which could then lead to instability gets smaller. At high Reynolds numbers it is agreed that fluid flows will be unstable. High Reynolds number can be achieved in several ways, e.g. if $\mu$ is a small value or if $\rho$ and $u$ are high values. This means that instabilities will arise almost immediately and the flow will become unstable or turbulent. 

\newpage


\section{A case study : The prey-predator model}

We are going to study a pair of first-order nonlinear differential equations, known as the \textbf{Lotka-Volterra equations}, also known as the predator-prey equations; frequently used to describe the dynamics of biological systems in which two species interact, one as a predator and the other as prey. \\ The populations will evolve with respect to each others through time; logically, if we define the number of preys by $x$ and the number of predators by $y$, when $y$ increases $x$ should decrease (more predators then fewer preys) and similiraly when $x$ decreases $y$ should decrease as well (fewer preys to eat then fewer predators). \\ \\ Thus, the populations change through time according to the pair of equations:

\begin{align}
\frac{dx}{dt} &= \alpha x - \beta x y, \\
\frac{dy}{dt} &= \delta x y - \gamma y
\end{align}


where
\begin{itemize}
	\item $x$ is the number of prey;
	\item $y$ is the number of some predators;
	\item $\frac{dx}{dt}$ and $\frac{dy}{dt}$ represent the instantaneous growth rates of the two populations;
	\item $\alpha, \beta, \gamma,$ and $\delta$ are positive real parameters describing the interaction of the two species.
\end{itemize}

\subsection{Stability of the equations}


The first step to analyse these equations is to find the population equilibrium, when neither of the population levels are changing, to do so we have to find where the derivatives of the two fuctions $x$ and $y$ cancel out.


	\begin{align}
	\frac{dx}{dt} &= \alpha x - \beta x y = 0 = \dot{x} (t), \\
	\frac{dy}{dt} &= \delta x y - \gamma y = 0 = \dot{y} (t)
	\end{align}
	
We have : 
\begin{center}
$\begin{cases} 
\alpha x - \beta x y = 0  \\ 
\delta x y - \gamma y = 0  \\ 
\end{cases}$
	
\end{center}

We then find two solutions to this system : 

\begin{center}
	$\Rightarrow$
	$\begin{cases} 
	x_{0} = y_{0} = 0  \\ 
	x_{1} = \dfrac{\gamma}{\delta}, y_{1} = \dfrac{\alpha}{\beta}\\ 
	\end{cases}$
\end{center}

\newpage

One is now able to check the stability of these two solutions, we let the opportunity to the reader to prove it, we will just state here that the solution \textbf{$(x_{0},y_{0})$ is not stable} whereas the solution \textbf{$(x_{1},y_{1})$ is stable}.


\subsection{Analysis of the evolution of the equations}

We want now to analyse the evolution of these two populations, and to see how the fixed point are represented in terms of instabilities. To begin with, one could plot the population dynamics of preys and predators

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.7\linewidth]{../Figure_2}
	\caption{Evolution of the populations over time }
	\label{fig:evo}
\end{figure}


The plot above is for an initial condition of two preys and one predator. We observe an oscillation in the number of preys and predators over time, in this case we reach a maximum of population around every 7.5t for both populations. We also observe that the mean value of the population is twice higher for the preys than the predators, when it is 1 for predators it is 2 for preys. \\ It is very interesting to notice that for these equations we have an oscillatory time dependence, we observe that when the number of preys start to decrease, the number of predators will also be soon decreasing. One can also note that the number of preys is maximum close to when the number of predators has reached its minimum, and then when the predator's population increases the population of preys starts to decrease as well; this makes sense with the intuitive idea that we have regarding preys and predators.

\newpage

We can now plot the equations as such we can observe the relative evolution of both populations. We do so by plotting the parametric equation of the population (predators versus preys), what we observe doing so are orbits.  

\begin{figure}[!htb]
	\begin{minipage}{0.48\textwidth}
		\centering
		\includegraphics[width=0.9\linewidth]{../initial_1_1}
		\caption{Initial population (x0,y0) = (1,1)}\label{Fig:Data1}
	\end{minipage}\hfill
	\begin{minipage}{0.48\textwidth}
		\centering
		\includegraphics[width=0.9\linewidth]{../Figure_1}
		\caption{Initial population (x0,y0) = (2,1)}\label{Fig:Data2}
	\end{minipage}
\end{figure}


\begin{figure}[!htb]
	\begin{minipage}{0.48\textwidth}
		\centering
		\includegraphics[width=0.9\linewidth]{../initial_1_4}
		\caption{Initial population (x0,y0) = (1,4)}\label{Fig:Data1}
	\end{minipage}\hfill
	\begin{minipage}{0.48\textwidth}
		\centering
		\includegraphics[width=0.9\linewidth]{../initial_12_3}
		\caption{Initial population (x0,y0) = (12,3)}\label{Fig:Data2}
	\end{minipage}
\end{figure}


We plotted the orbits for different initial populations. We observe that as the population increases, the orbits spread more and more, this is due to the fact that if we start with a high number of people composing the populattion, this number will quickly increase. Furthermore, we see that no matter the initial conditions, the number of preys is always higher than the number of predators, this statement is in part due to the parameters we set at the beginning ($\alpha$, $\beta$, $\gamma$ and $\delta$). Nevertheless, the important fact of these orbits, is that the two population will always be interconnected and will evolve with respect to each others.



\newpage




Now, one can rewrite the Lotka-Volterra equations as follow : 


\begin{align}
\frac{dy}{dx} = \frac{y}{x} \frac{\delta x - \gamma}{\beta y - \alpha}
\end{align}


From this equation, what we observe is that the solutions are closed curves. If we integrate this equation : 

\begin{align}
\frac{\beta y - \alpha}{y} dy + \frac{\delta x - \gamma}{x} dx = 0 \Longleftrightarrow
(\beta - \frac{\alpha}{y}) dy + (\delta - \frac{\gamma}{x}) dx = 0
\end{align}

It yields to : 

\begin{align}
V = \delta x - \gamma ln(x) + \beta y - \alpha ln(y)
\end{align}

where $V$ is a constant quantity - a potential - depending on the initial conditions and conserved on each curve. We can then plot these closed curves called isolines;


\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.7\linewidth]{../Figure_iso}
	\caption{Isolines for different potential value $V$}
	\label{Fig:iso}
\end{figure}


\newpage

\section{Turbulences arising from the motion of aircrafts }

\subsection{Clear-air turbulence : an unpredictable phenomenon}

Clear-air turbulence (CAT) presents a scientific problem of considerable importance to aviation and to attempts to understand atmospheric processes that must be incorporated in a long range numerical prediction model.
\\ If you have ever taken the plane, you might have already experienced CAT, during your flight travel, the tremors you felt are caused by CAT. This phenomenon is the turbulent movement of air masses in the absence of any visual clues, such as clouds, and is caused when bodies of air moving at widely different speeds meet. \\ \\ We will give a few features of CAT. There are two types of CAT : mechanical (disruption to smooth horizontal flow) or thermal (turbulence caused by vertical currents of air in an unstable atmosphere); there are mainly caused by Jet Stream (fast moving current of air) or terrain, big clusters of clouds can also create CAT.\\

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.7\linewidth]{../fig/62708_58692_800_auto_jpg}
	\caption{The two types of CAT : mechanical (Horizontal) and thermal (Vertical) }
	\label{Fig:CAT}
\end{figure}



\subsection{Wake turbulences : pertubations created by the aircraft}


Wake turbulence is a disturbance in the atmosphere that forms behind an aircraft as it passes through the air. It includes various components, the most important of which are wingtip vortices and jetwash. Jetwash refers simply to the rapidly moving gases expelled from a jet engine; it is extremely turbulent, but of short duration. Wingtip vortices, on the other hand, are much more stable and can remain in the air for up to three minutes after the passage of an aircraft.

\newpage

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.7\linewidth]{../fig/Airplane_vortex_edit}
	\caption{Visualization of Wake Turbulences behind an aircraft}
	\label{Fig:waketurbulence}
\end{figure}


\subsubsection{Wingtip Vortices}

Wingtip vortices form the primary component of wake turbulence. Depending on ambient atmospheric humidity as well as the geometry and wing loading of aircraft, water may condense or freeze in the core of the vortices, making the vortices visible. Wingtip vortices occur when a wing is generating lift. Air from below the wing is drawn around the wingtip into the region above the wing by the lower pressure above the wing, causing a vortex to trail from each wingtip. The strength of wingtip vortices is determined primarily by the weight and airspeed of the aircraft. 


\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.7\linewidth]{../1-vortex}
	\caption{Pressure Gradient around the wing of a plane}
	\label{Fig:wingtip}
\end{figure}

\newpage

\section{Conclusion}

Throughout this document, we have tackled two problems in turbulence and instability studies : one concerning the dynamic of population and the other one concerning motion of aircrafts and fluid turbulences. As a conclusion, we can extract and detail a few features of turbulent flows. Thanks to the wake turbulences we can easily say that turbulent flow are irregular, which means that deterministic solution are hardly found, that's why we use statistics. One important property that we also observe is the high diffusivity of a turbulent flow, it will tend to increase the mixing of fluid mixtures, this effect is usually described by a diffusion coefficient. Another property of turbulent flows is their extremely high dissipation rate in the absence of any persistent source of energy supply; this rapid dissipation is due to the conversion of kinetic energy into internal energy by viscous shear stress. We could also note an important effect of rotation in turbulent flows. \\ \\
To solve these problems and to analyse turbulent flows, one could use the concept of energy cascade where the turbulent flows are considered as a superposition of a spectrum of flow velocities i.e. we consider the transfer of energy from large scales of motion to the small scales. We then define what we call Kolmogorov lengths and time scales which are the smallest scales, scales where the viscosity dominates and the turbulent kinetic energy is dissipated into heat.
To conclude, although it is possible to find some particular solutions of the NavierStokes equations governing fluid motion, all such solutions are unstable to finite perturbations at large Reynolds numbers. Sensitive dependence on the initial and boundary conditions makes fluid flow irregular both in time and in space so that a statistical description is needed. \\ 
The problem is extremely relevant to physics and science studies, nevertheless it is still partially unresolved.\\ We could finish with the story which tells that Werner Heisenberg was asked one day what he would ask God, given the opportunity. His reply was: "When I meet God, I am going to ask him two questions: Why relativity? And why turbulence? I really believe he will have an answer for the first."



\newpage

\nocite{din2013dynamics}
\nocite{zheng2001apparatus}
\nocite{anderson2000experimental}
\nocite{churchfield2012numerical}
\nocite{danca1997detailed}
\bibliographystyle{jfm}
\bibliography{./biblio}







\end{document}



