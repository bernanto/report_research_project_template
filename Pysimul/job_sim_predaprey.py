"""
Start ipython with the command `ipython --matplotlib`

Then, run this script with:

```
run job_sim_predaprey.py
```
"""
import matplotlib.pyplot as plt
from pathlib import Path
from fluidsim.solvers.models0d.predaprey.solver import Simul

here = Path(__file__).absolute().parent

params = Simul.create_default_params()

params.time_stepping.deltat0 = 0.001
params.time_stepping.t_end = 40

params.output.periods_print.print_stdout = 0.1

sim = Simul(params)

sim.state.state_phys.set_var("X", sim.Xs + 1)
sim.state.state_phys.set_var("Y", sim.Ys + 4)

# sim.output.phys_fields.plot()
sim.time_stepping.start()

sim.output.print_stdout.plot_XY()

# Note: if you want to modify the figure and/or save it, you can use
#ax = plt.gca()
#fig = ax.figure

sim.output.print_stdout.plot_XY_vs_time()

#tmp_dir = here.parent / "tmp"
#tmp_dir.mkdir(exist_ok=True)

#fig.savefig(tmp_dir / "fig_preypreda.png")

plt.show()
